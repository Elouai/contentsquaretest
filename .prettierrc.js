module.exports = {
  arrowParens: 'always',
  printWidth: 140,
  singleQuote: true,
  trailingComma: 'es5',
};
