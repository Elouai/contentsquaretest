import { squareNavigation, cardinalDirectionRotation } from '../src/contentSquare';
import { Direction, Instruction, Position, Coordinates } from '../src/types';

describe('ContentSquare ', () => {
  describe('CardinalDirectionRotation', () => {
    it('should return E when given N anf L instruction', async () => {
      // Given
      const direction: Direction = 'N';
      const instruction: Instruction = 'L';
      // When
      const result = cardinalDirectionRotation(direction, instruction);

      // Then
      expect(result).toEqual('E');
    });

    it('should return N when given W anf L instruction', async () => {
      // Given
      const direction: Direction = 'W';
      const instruction: Instruction = 'L';
      // When
      const result = cardinalDirectionRotation(direction, instruction);

      // Then
      expect(result).toEqual('N');
    });

    it('should return W when given N anf R instruction', async () => {
      // Given
      const direction: Direction = 'N';
      const instruction: Instruction = 'R';
      // When
      const result = cardinalDirectionRotation(direction, instruction);

      // Then
      expect(result).toEqual('W');
    });

    it('should return S when given W anf R instruction', async () => {
      // Given
      const direction: Direction = 'W';
      const instruction: Instruction = 'R';
      // When
      const result = cardinalDirectionRotation(direction, instruction);

      // Then
      expect(result).toEqual('S');
    });

    it('should return old direction when is incorrect ', async () => {
      // Given
      const direction: Direction = 'X';
      const instruction: Instruction = 'R';
      // When
      const result = cardinalDirectionRotation(direction, instruction);

      // Then
      expect(result).toEqual('X');
    });
  });

  describe('SquareNavigation', () => {
    it('should return correct position if not moving', async () => {
      // Given
      const limit: Position = { x: 5, y: 5 };
      const road = ['L', 'L', 'L'];
      const coordinate: Coordinates = { x: 1, y: 2, direction: 'N' };

      // When
      const result = squareNavigation({ limit, road, coordinate });

      // Then
      expect(result.x).toEqual(coordinate.x);
      expect(result.y).toEqual(coordinate.y);
      expect(result.direction).toEqual('W');
    });

    it('should ignore incorrect instruction', async () => {
      // Given
      const limit: Position = { x: 5, y: 5 };
      const road = ['L', 'L', 'L', 'Y', 'T'];
      const coordinate: Coordinates = { x: 1, y: 2, direction: 'N' };

      // When
      const result = squareNavigation({ limit, road, coordinate });

      // Then
      expect(result.x).toEqual(coordinate.x);
      expect(result.y).toEqual(coordinate.y);
      expect(result.direction).toEqual('W');
    });

    it('should respond with correct position', async () => {
      // Given
      const limit: Position = { x: 5, y: 5 };
      const road = ['F', 'F', 'F', 'F', 'F'];
      const coordinate: Coordinates = { x: 1, y: 2, direction: 'N' };

      // When
      const result = squareNavigation({ limit, road, coordinate });

      // Then
      expect(result.x).toEqual(coordinate.x);
      expect(result.y).toEqual(5);
      expect(result.direction).toEqual(coordinate.direction);
    });
  });
});
