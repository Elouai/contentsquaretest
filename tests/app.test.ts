import request, { SuperTest, Test } from 'supertest';
import { app } from '../src/app';

describe('GET /health', () => {
  let agent: SuperTest<Test>;

  beforeAll(async () => {
    agent = request(app);
  });

  it('should return proposal list when query string is empty', async () => {
    // Given
    // When
    const response = await agent.get('/health').expect(200);

    // Then
    const result = JSON.parse(response.text);
    expect(result.status).toEqual('ok');
  });
});
