# contentsquaretest

## Available scripts

- `clean` - remove coverage data, Jest cache and transpiled files,
- `build` - transpile TypeScript to ES6,
- `dev` - interactive watch mode to automatically transpile source files,
- `lint` - lint source files and tests,
- `format` - format source files and tests,
- `format` - format source files and tests,
- `prod` - server application after build,
- `start` - server application flowing the variable environment \$APP_ENV,
- `test` - run tests
- `test:watch` - interactive watch mode to automatically re-run tests


You can test with postman collection 

![](postman/image.png)
