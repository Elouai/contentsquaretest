import express from 'express';
import bodyParser from 'body-parser';
import multer from 'multer';
import fs from 'fs';
import { squareNavigation } from './contentSquare';
import { fileToObject } from './utils';

const upload = multer({
  dest: 'uploads/',
});

export const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/health', (req, res) => {
  res.json({
    status: 'ok',
  });
});

app.post('/ContentSquare', upload.single('mowerFile'), (req, res) => {
  try {
    let data = '';
    const readStream = fs.createReadStream(req.file.path, 'utf8');

    readStream
      .on('data', (chunk) => {
        data += chunk;
      })
      .on('end', () => {
        const result = fileToObject(data);
        res.json(
          result.map((el) => {
            return squareNavigation(el);
          })
        );
      });
  } catch (e) {
    res.status(400).json({
      message: 'Error reading the file !',
    });
  }
});
