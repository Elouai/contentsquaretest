export type Instruction = 'R' | 'L' | 'F' | string;

export type Direction = 'N' | 'E' | 'W' | 'S' | string;

export type Position = {
  x: number;
  y: number;
};

export interface Coordinates extends Position {
  direction: Direction;
}

export interface ContentSquare {
  coordinate: Coordinates;
  limit: Position;
  road: Array<Instruction>;
}
