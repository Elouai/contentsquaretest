/* eslint-disable no-param-reassign */
import { Direction, Coordinates, ContentSquare } from './types';

const CardinalDirection: [Direction] = ('NESW' as unknown) as [Direction];

export const cardinalDirectionRotation = (direction: Direction, instruction: string): Direction => {
  const current = CardinalDirection.indexOf(direction);
  if (current === -1) return direction;

  if (instruction === 'L') {
    const leftPosition = current + 1;
    if (leftPosition >= CardinalDirection.length) {
      return CardinalDirection[0];
    }
    return CardinalDirection[leftPosition];
  }

  const rightPosition = current - 1;
  if (rightPosition < 0) {
    return CardinalDirection[CardinalDirection.length - 1];
  }
  return CardinalDirection[rightPosition];
};

export const squareNavigation = ({ coordinate, limit, road }: ContentSquare): Coordinates => {
  if (road.length <= 0) {
    return coordinate;
  }
  const instruction = road.shift();
  if (instruction === 'L' || instruction === 'R') {
    coordinate.direction = cardinalDirectionRotation(coordinate.direction, instruction);
  }
  if (instruction === 'F') {
    const orientation = 'NS'.includes(coordinate.direction) ? 'y' : 'x';
    if (coordinate.direction === 'N' || coordinate.direction === 'E') {
      if (limit[orientation] >= coordinate[orientation] + 1) ++coordinate[orientation];
    }
    if (coordinate.direction === 'S' || coordinate.direction === 'W') {
      if (coordinate[orientation] - 1 >= 0) --coordinate[orientation];
    }
  }

  return squareNavigation({ coordinate, limit, road });
};
