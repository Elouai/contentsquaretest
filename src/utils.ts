import { Position, ContentSquare } from './types';

export const fileToObject = (data: string): ContentSquare[] => {
  const result = data.split('\n');

  const position: Position = { x: parseInt(result[0][0], 10), y: parseInt(result[0][2], 10) };

  result.shift();
  const coordinates: ContentSquare[] = [];

  for (let i = 0; i < result.length; i += 2) {
    const holder = result[i].replace(/\s/g, '');
    coordinates.push({
      coordinate: { x: parseInt(holder[0], 10), y: parseInt(holder[1], 10), direction: holder[2] },
      road: [...result[i + 1]],
      limit: position,
    });
  }

  return coordinates;
};
